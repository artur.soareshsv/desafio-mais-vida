package br.com.artur.soreshsv.desafio.repository;

import br.com.artur.soreshsv.desafio.domain.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.support.MergingPersistenceUnitManager;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MunicipioRepository extends JpaRepository<Municipio, Integer> {
    List<Municipio> findByEstadoId(Integer id);
}
