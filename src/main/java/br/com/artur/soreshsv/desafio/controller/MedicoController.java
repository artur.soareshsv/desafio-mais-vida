package br.com.artur.soreshsv.desafio.controller;

import br.com.artur.soreshsv.desafio.domain.Medico;
import org.springframework.web.bind.annotation.*;
import br.com.artur.soreshsv.desafio.service.MedicoService;

import javax.inject.Inject;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    @Inject
    private MedicoService service;

    @PostMapping
    private Optional<Medico> save(@RequestBody Medico medico) {
        return this.service.save(medico);
    }

    @GetMapping
    private List<Medico> getMedicos() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    private Medico getMedicos(@PathVariable Integer id) {
        return this.service.getMedico(id).orElse(null);
    }
}
