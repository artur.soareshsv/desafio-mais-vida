package br.com.artur.soreshsv.desafio.enumeretor;

import br.com.artur.soreshsv.desafio.util.StatusDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;

import java.util.Arrays;

@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = StatusDeserializer.class)
public enum Status {
    OCUPADO(1),
    DISPONIVEL(2);

    private Integer status;

    Status(Integer status) {
        this.status = status;
    }

    public static Status buscarPorStatus(int id) {
        return Arrays.stream(Status.values()).filter(
                (s) -> s.getStatus().equals(id)
        ).findFirst().get();
    }
}

