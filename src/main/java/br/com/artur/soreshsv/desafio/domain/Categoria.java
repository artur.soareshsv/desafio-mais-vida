package br.com.artur.soreshsv.desafio.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tab_categoria")
public class Categoria implements Serializable {

    @Id
    @Column(name = "cod_categoria")
    private Integer id;

    @Column(name = "dsc_nome")
    private String nome;
}
