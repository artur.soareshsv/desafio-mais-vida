package br.com.artur.soreshsv.desafio.repository;

import br.com.artur.soreshsv.desafio.domain.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {
}
