package br.com.artur.soreshsv.desafio.service;

import br.com.artur.soreshsv.desafio.domain.Medico;
import org.springframework.stereotype.Service;
import br.com.artur.soreshsv.desafio.repository.MedicoRepository;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@Service
public class MedicoService {

    @Inject
    private MedicoRepository repository;

    public Optional<Medico> save(Medico medico) {
        return Optional.of(this.repository.save(medico));
    }

    public List<Medico> findAll() {
        return this.repository.findAll();
    }

    public Optional<Medico> getMedico(Integer id) {
        return Optional.of(this.repository.findOne(id));
    }
}
