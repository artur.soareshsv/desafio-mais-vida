package br.com.artur.soreshsv.desafio.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tab_municipio")
public class Municipio implements Serializable {

    @Id
    @Column(name = "cod_municipio")
    private Integer id;

    @Column(name = "dsc_nome")
    private String nome;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cod_estado")
    private Estado estado;
}
