package br.com.artur.soreshsv.desafio.repository;

import br.com.artur.soreshsv.desafio.domain.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by artur on 17/05/17.
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
}
