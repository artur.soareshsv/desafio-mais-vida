package br.com.artur.soreshsv.desafio.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tab_estado")
public class Estado implements Serializable {

    @Id
    @Column(name = "cod_estado")
    private Integer id;

    @Column(name = "dsc_nome")
    private String nome;

    @Column(name = "dsc_uf")
    private String uf;
}
