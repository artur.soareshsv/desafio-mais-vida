package br.com.artur.soreshsv.desafio.controller;

import br.com.artur.soreshsv.desafio.domain.Estado;
import br.com.artur.soreshsv.desafio.domain.Municipio;
import org.springframework.web.bind.annotation.*;
import br.com.artur.soreshsv.desafio.repository.EstadoRepository;
import br.com.artur.soreshsv.desafio.repository.MunicipioRepository;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/localidade")
public class LocalidadeController {
    @Inject
    private EstadoRepository estadoRepository;

    @Inject
    private MunicipioRepository municipioRepository;

    @GetMapping("/estados")
    private List<Estado> getEstados() {
        return this.estadoRepository.findAll();
    }

    @GetMapping("/municipios/{id}")
    private List<Municipio> getMunicipios(@PathVariable Integer id) {
        return this.municipioRepository.findByEstadoId(id);
    }
}
