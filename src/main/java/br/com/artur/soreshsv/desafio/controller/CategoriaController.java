package br.com.artur.soreshsv.desafio.controller;

import br.com.artur.soreshsv.desafio.domain.Categoria;
import br.com.artur.soreshsv.desafio.repository.CategoriaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/especialidade")
public class CategoriaController {

    @Inject
    private CategoriaRepository repository;

    @GetMapping
    private List<Categoria> getEspecialidades() {
        return this.repository.findAll();
    }
}
