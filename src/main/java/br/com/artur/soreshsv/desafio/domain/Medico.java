package br.com.artur.soreshsv.desafio.domain;

import br.com.artur.soreshsv.desafio.enumeretor.Status;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tab_medico")
public class Medico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_medico")
    private Integer id;

    @Column(name = "dsc_nome")
    private String nome;

    @Column(name = "dsc_sobrenome")
    private String sobrenome;

    @Column(name = "dsc_email")
    private String email;

    @Column(name = "bln_ativo")
    private Boolean ativo;

    @Enumerated(EnumType.STRING)
    @Column(name = "dsc_status")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "cod_municipio")
    private Municipio municipio;

    @ManyToOne
    @JoinColumn(name = "cod_categoria")
    private Categoria categoria;
}
