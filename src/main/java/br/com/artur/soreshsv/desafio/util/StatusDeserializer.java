package br.com.artur.soreshsv.desafio.util;

import br.com.artur.soreshsv.desafio.enumeretor.Status;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StatusDeserializer extends StdDeserializer<Status> {
    public StatusDeserializer() {
        this(null);
    }

    public StatusDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Status deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        Number id = ((IntNode) node).numberValue();
        return Status.buscarPorStatus(id.intValue());
    }
}
